import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

CustomKeywords.'com.ea.utilities.CustomKey.openURL'(GlobalVariable.UAT_URL_SG)
WebUI.click(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('aTag', ' here '))
WebUI.delay(2)
//WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('logoSg_UAT', ''))
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('h3Tag', 'Forget Password ?'))
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('pTag', ' Enter your e-mail address below to reset your password. '))
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('backButton', 'Back '))
WebUI.setText(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('textBox', 'Email'), 'gmedestest+01303@gmail.com')
WebUI.click(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('button', ' Submit '))
WebUI.delay(2)
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('textItem', ' Please check email to get reset password link. '))
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('h3Tag', 'Login to your account'))
WebUI.closeWindowTitle('G-MEDS')

