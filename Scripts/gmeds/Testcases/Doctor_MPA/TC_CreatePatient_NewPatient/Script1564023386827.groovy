import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

CustomKeywords.'com.ea.utilities.CustomKey.navigateSubMenu'('Patients', 'Register New Patient')
WebUI.delay(1)
WebUI.click(findTestObject('Doctor_MPA/Patients/Confirm_Checkbox1'))
WebUI.click(findTestObject('Doctor_MPA/Patients/Confirm_Checkbox2'))
CustomKeywords.'com.ea.utilities.CustomKey.selectDropdown'('Mr.', 'Mrs.') // Title
WebUI.setText(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('textBox_noName', 'utilbundle_patient_personalInformation_firstName'), 'Testname' ) //first name
WebUI.setText(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('textBox_noName', 'utilbundle_patient_personalInformation_lastName'), 'Testlastname' ) // family name

//Date of Birth
CustomKeywords.'com.ea.utilities.CustomKey.selectDropdown'('Day', '01')
CustomKeywords.'com.ea.utilities.CustomKey.selectDropdown'('Month', '01')
CustomKeywords.'com.ea.utilities.CustomKey.selectDropdown'('Year', '1960')

//Gender
WebUI.click(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('radioButton', 'Male\n\n'))

//Country
CustomKeywords.'com.ea.utilities.CustomKey.selectDropdown'('Singapore', 'Singapore')

//NO caregiver
WebUI.setText(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('textBox_noName', 'utilbundle_patient_personalInformation_emailAddress'), 'gmedestest1@gmail.com' )

//CustomKeywords.'com.ea.utilities.CustomKey.selectDropdown'('Singapore', 'Singapore')
WebUI.setText(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('textBox_noName', 'utilbundle_patient_phones_0_number'), '1234567' )

//Patients ID
WebUI.setText(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('textBox_noName', 'utilbundle_patient_personalInformation_passportNo'), 'P123456' )
//CustomKeywords.'com.ea.utilities.CustomKey.selectDropdown'('Singapore', 'Singapore')
//CustomKeywords.'com.ea.utilities.CustomKey.selectDropdown'('Singapore', 'Singapore')

//Allergy
WebUI.click(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('radioButton', ' No\n\n\n'))
//WebUI.scrollToElement(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('aTag', ' Save '), 2)

//Diagnosis
WebUI.click(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('textItem', 'Arthritis'))


//Click save
WebUI.click(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('aTag', ' Save '))

WebUI.delay(3)

//Check new patient
WebUI.setText(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('searchBox', 'Search by Patient Name/Code'), 'Testname')
WebUI.sendKeys(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('searchBox', 'Search by Patient Name/Code'), Keys.chord(Keys.ENTER))
WebUI.delay(1)
WebUI.verifyElementVisible(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('anyText', 'Testname'))

//Delete
WebUI.click(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('aTag', 'Delete '))
WebUI.delay(1)
WebUI.click(CustomKeywords.'com.ea.utilities.CustomKey.itemXpath'('aTag', 'Delete'))


