import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable


WebUI.switchToWindowIndex(1) //to switch to a pop up new window
WebUI.switchToWindowTitle(null, FailureHandling.CONTINUE_ON_FAILURE)
WebUI.switchToWindowUrl(null, FailureHandling.CONTINUE_ON_FAILURE)


//Test data - access testData using for loop

for(def row = 1; row <= findTestData('null').getRowNumbers(); row ++){
		findTestData('null').getValue('username', row) //to get date from test data	
	
}


//Mouse over for mouse over object selection

WebUI.mouseOver(findTestObject('null'))

