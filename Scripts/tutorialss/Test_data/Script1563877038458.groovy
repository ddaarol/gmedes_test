import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl(GlobalVariable.UAT_URL_SG)

WebUI.setText(CustomKeywords.'com.ea.utilities.CustomKey.ItemXpath'('textbox', 'Username'), findTestData('Login').getValue('Username', 1))
//WebUI.setText(CustomKeywords.'com.ea.utilities.CustomKey.ItemXpath'('textbox', 'Username'), findTestData('Login').getValue(1, 1))
WebUI.setText(CustomKeywords.'com.ea.utilities.CustomKey.ItemXpath'('textbox', 'Password'), findTestData('Login').getValue('Password', 1))
//WebUI.setText(CustomKeywords.'com.ea.utilities.CustomKey.ItemXpath'('textbox', 'Password'), findTestData('Login').getValue(2, 1))

WebUI.click(CustomKeywords.'com.ea.utilities.CustomKey.ItemXpath'('button', 'Login'))

WebUI.delay(3)

